#! /bin/bash
set -e -x
env

pylint -f parseable --rcfile pylint.rc "$@" | tee pylint.out || /bin/true
