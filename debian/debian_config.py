# This file is sourced by "execfile" from scrutiny.settings

import os

PROJECT_NAME = 'scrutiny'

DEBUG = False
TEMPLATE_DEBUG = False

SECRET_KEY = open('/etc/%s/secret' % PROJECT_NAME).read()

ADMINS = (('Tous', 'root@localhost'),)

EMAIL_SUBJECT_PREFIX = '[%s] ' % PROJECT_NAME

ETC_DIR = '/etc/%s' % PROJECT_NAME
VAR_DIR = '/var/lib/%s' % PROJECT_NAME

# collecstatic destination
STATIC_ROOT = os.path.join(VAR_DIR, 'collectstatic')

MEDIA_ROOT = os.path.join(VAR_DIR, 'media')

# Browsers may ensure that cookies are only sent under an HTTPS connection
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SESSION_EXPIRE_AT_BROWSER_CLOSER = True
SESSION_COOKIE_AGE = 36000  # 10h

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': PROJECT_NAME,
    }
}

#
# local settings
#
exec(open(os.path.join(ETC_DIR, 'settings.py')).read())
