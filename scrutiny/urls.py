from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

import scrutiny.views
from scrutiny.projects.urls import urlpatterns as projects_urls

admin.autodiscover()

urlpatterns = [
    path('', scrutiny.views.home, name='home'),
    path('projects/', include(projects_urls)),
    re_path(r'^admin/', admin.site.urls),
    path('logout/', scrutiny.views.logout, name='auth_logout'),
    path('login/', scrutiny.views.login, name='auth_login'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/mellon/', include('mellon.urls')))

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar  # pylint: disable=import-error

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
