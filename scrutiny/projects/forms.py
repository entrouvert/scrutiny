from django import forms

from .models import IssueInfo


class IssueInfoForm(forms.ModelForm):
    class Meta:
        model = IssueInfo
        fields = [
            'issue_type',
            'doc_focus',
            'wording',
        ]
