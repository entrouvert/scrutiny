from django import template

register = template.Library()


@register.simple_tag
def installed_version(module, platform, service):
    v = module.get_installed_version(platform, service)
    if v is None:
        return ''
    return v.version.version


@register.filter(name='get')
def get(obj, key):
    try:
        return obj.get(key)
    except AttributeError:
        try:
            return obj[key]
        except (IndexError, KeyError, TypeError):
            return None


@register.simple_tag
def service_url(platform, service):
    installed_service = service.get_installed_service(platform=platform)
    if installed_service is None:
        return ''
    return installed_service.url
