from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='InstalledService',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('url', models.URLField()),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InstalledVersion',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('timestamp', models.DateTimeField()),
                ('service', models.ForeignKey(to='projects.InstalledService', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=50)),
                ('repository_url', models.URLField()),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Platform',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('project', models.ForeignKey(to='projects.Project', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('version', models.CharField(max_length=50)),
                ('module', models.ForeignKey(to='projects.Module', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='platform',
            name='project',
            field=models.ForeignKey(to='projects.Project', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='installedversion',
            name='version',
            field=models.ForeignKey(to='projects.Version', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='installedservice',
            name='platform',
            field=models.ForeignKey(to='projects.Platform', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='installedservice',
            name='service',
            field=models.ForeignKey(to='projects.Service', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
