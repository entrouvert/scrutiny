from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('projects', '0005_auto_20201216_1435'),
    ]

    operations = [
        migrations.CreateModel(
            name='IssueInfo',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('issue_id', models.IntegerField()),
                (
                    'issue_type',
                    models.CharField(
                        choices=[
                            ('NEW', 'NEW (Nouveautés)'),
                            ('BUGFIX', 'BUGFIX (Corrections)'),
                            ('DEV', 'DEV (Développements)'),
                            ('TECH', 'TECH (À ignorer)'),
                        ],
                        max_length=10,
                        null=True,
                        verbose_name='Type de ticket',
                    ),
                ),
                (
                    'doc_focus',
                    models.BooleanField(
                        default=False, verbose_name='Attention à porter à la doc (la mettre à jour ?)'
                    ),
                ),
            ],
        ),
    ]
