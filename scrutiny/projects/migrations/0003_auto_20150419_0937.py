from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('projects', '0002_auto_20150325_0908'),
    ]

    operations = [
        migrations.AlterField(
            model_name='version',
            name='version',
            field=models.CharField(max_length=50, blank=True),
            preserve_default=True,
        ),
    ]
