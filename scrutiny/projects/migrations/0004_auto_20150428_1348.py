from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('projects', '0003_auto_20150419_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='version',
            name='version',
            field=models.CharField(max_length=100, blank=True),
            preserve_default=True,
        ),
    ]
