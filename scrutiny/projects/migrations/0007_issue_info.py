from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('projects', '0006_issue_info'),
    ]

    operations = [
        migrations.AddField(
            model_name='issueinfo',
            name='wording',
            field=models.TextField(blank=True, verbose_name='Description pour les notes de mise à jour'),
        ),
    ]
