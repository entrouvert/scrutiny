import redmine
from django.conf import settings
from django.core.management.base import BaseCommand
from redmine import Redmine

from scrutiny.projects.models import Module
from scrutiny.projects.utils import get_issue_deployment_status


class Command(BaseCommand):
    def handle(self, verbosity, *args, **options):
        redmine_server = Redmine(
            settings.REDMINE_URL, key=settings.REDMINE_API_KEY, impersonate=settings.REDMINE_IMPERSONATE
        )
        RESOLVED_ID = None
        DEPLOYED_ID = None

        for issue_status in redmine_server.issue_status.all():
            if issue_status.name == 'Résolu (à déployer)':
                RESOLVED_ID = issue_status.id
            elif issue_status.name == 'Solution déployée':
                DEPLOYED_ID = issue_status.id

        for module in Module.objects.all():
            if not module.repository_url:
                continue

            try:
                issues = list(
                    redmine_server.issue.filter(project_id=module.redmine_project, status_id=RESOLVED_ID)
                )
            except redmine.exceptions.ResourceNotFoundError:
                if verbosity > 1:
                    print('unknown redmine module:', module.name)
                continue
            for issue in issues:
                deployment_status = get_issue_deployment_status(issue.id)
                if not deployment_status.get('platforms'):
                    if verbosity > 1:
                        print(
                            '  unknown status for https://dev.entrouvert.org/issues/%s (%s)'
                            % (issue.id, module.redmine_project)
                        )
                    continue
                platforms_ok = [
                    x.rsplit(' / ', 1)[0]
                    for x in deployment_status['platforms'].keys()
                    if deployment_status['platforms'][x]['status'] == 'ok'
                ]
                if settings.REDMINE_REFERENCE_PLATFORM in platforms_ok:
                    if verbosity > 0:
                        print(
                            '%s - https://dev.entrouvert.org/issues/%s' % (module.redmine_project, issue.id)
                        )
                    issue.status_id = DEPLOYED_ID
                    issue.save()
