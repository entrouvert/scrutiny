from django.urls import path, re_path

from .views import (
    IssueInfoUpdate,
    IssuesSnippet,
    ModuleDeploymentsView,
    ModuleDiffView,
    ModuleIssuesView,
    ModulesView,
    ProjectDetailView,
    ProjectHistoryView,
    ProjectSummaryHistoryDayView,
    ProjectSummaryHistoryView,
    api_issues_json,
    module_deployments_json,
)

urlpatterns = [
    path('modules/', ModulesView.as_view(), name='modules-list'),
    re_path(r'^(?P<slug>[\w,-]+)/$', ProjectDetailView.as_view(), name='project-view'),
    re_path(
        r'^(?P<slug>[\w,-]+)/history$', ProjectSummaryHistoryView.as_view(), name='project-summary-history'
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/history/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)$',
        ProjectSummaryHistoryDayView.as_view(),
        name='project-summary-history-day',
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/history/future$',
        ProjectSummaryHistoryDayView.as_view(),
        name='project-summary-history-future',
    ),
    re_path(r'^(?P<slug>[\w,-]+)/detailed-history$', ProjectHistoryView.as_view(), name='project-history'),
    re_path(
        r'^modules/(?P<name>[\w,-]+)/diff/(?P<commit1>[\w,\.-]+)/(?P<commit2>[\w,\.-]+)$',
        ModuleDiffView.as_view(),
        name='module-diff',
    ),
    path('issues/snippet/', IssuesSnippet.as_view(), name='issues-snippet'),
    path(
        'issues/<int:issue_id>/info',
        IssueInfoUpdate.as_view(),
        name='issue-edit-info',
    ),
    re_path(
        r'^modules/(?P<name>[\w,-]+)/issues/(?P<commit1>[\w,\.-]+)/(?P<commit2>[\w,\.-]+)$',
        ModuleIssuesView.as_view(),
        name='module-issues',
    ),
    re_path(r'^modules/(?P<name>[\w,-]+)/$', ModuleDeploymentsView.as_view(), name='module-deployments'),
    re_path(r'^modules/(?P<name>[\w,-]+)/json$', module_deployments_json, name='module-deployments-json'),
    path('api/issues/<int:issue>/', api_issues_json),
]
