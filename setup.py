#! /usr/bin/env python3

import os
import subprocess
from distutils.cmd import Command
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.errors import CompileError
from distutils.spawn import find_executable

from setuptools import find_packages, setup


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class compile_scss(Command):
    description = 'compile scss files into css files'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sass_bin = None
        for program in ('sassc', 'sass'):
            sass_bin = find_executable(program)
            if sass_bin:
                break
        if not sass_bin:
            raise CompileError(
                'A sass compiler is required but none was found.  See sass-lang.com for choices.'
            )

        for package in self.distribution.packages:
            for package_path in __import__(package).__path__:
                for path, dirnames, filenames in os.walk(package_path):
                    for filename in filenames:
                        if not filename.endswith('.scss'):
                            continue
                        if filename.startswith('_'):
                            continue
                        subprocess.check_call(
                            [
                                sass_bin,
                                '%s/%s' % (path, filename),
                                '%s/%s' % (path, filename.replace('.scss', '.css')),
                            ]
                        )


class build(_build):
    sub_commands = [('compile_scss', None)] + _build.sub_commands


setup(
    name='scrutiny',
    version=get_version(),
    license='AGPLv3',
    description='',
    author="Entr'ouvert",
    author_email='info@entrouvert.com',
    packages=find_packages(os.path.dirname(__file__) or '.'),
    scripts=['manage.py'],
    include_package_data=True,
    install_requires=[
        'django>=3.2, <3.3',
        'requests',
        'gadjo',
    ],
    cmdclass={
        'build': build,
        'compile_scss': compile_scss,
        'sdist': eo_sdist,
    },
)
